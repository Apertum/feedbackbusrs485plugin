/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.fbus485.core;

import ru.apertum.qsystem.common.Uses;
import ru.apertum.qsystem.common.cmd.AJsonRPC20;
import ru.apertum.qsystem.common.cmd.CmdParams;
import ru.apertum.qsystem.common.cmd.JsonRPC20Error;
import ru.apertum.qsystem.extra.ITask;
import ru.apertum.qsystem.server.controller.Executer;
import ru.apertum.qsystem.server.model.QUser;
import ru.apertum.qsystem.server.model.QUserList;

/**
 *
 * @author Evgeniy Egorov
 */
public class ButtonDevice extends Object {

    public final String addres;
    public final Long userId;
    public final QUser user;

    public ButtonDevice(Long userId, String addres) {
        this.addres = addres;
        this.userId = userId;
        user = QUserList.getInstance().getById(userId);

        mess[0] = 1;
        mess[1] = addres.getBytes()[0];
        mess[mess.length - 1] = 7;

        System.out.println("Button created: adress=" + addres + "; user id = " + userId + "; user name=" + user.getName());
    }

    @Override
    public String toString() {
        return user == null ? String.valueOf(userId) : user.getName();
    }

    /*
     * mess[2] = 
     * 0x30 – светодиод погашен;
     0x31 – включен Красный;
     0x32 – включен Зеленый;
     0x33 – мигает Красный (200 мс);
     0x34 – мигает Зеленый (200 мс);
     0x35 – мигает Красный (500 мс);
     0x36 – мигает Зеленый (500 мс);
     0x37 – писк (500 мс) + светодиод погашен;
     0x38 – писк (500 мс) + включен Красный;
     0x39 – писк (500 мс) + включен Зеленый;
     0x3A – писк (500 мс) + мигает Красный (200 мс);
     0x3B – писк (500 мс) + мигает Зеленый (200 мс);
     0x3C – писк (500 мс) + мигает Красный (500 мс);
     0x3D – писк (500 мс) + мигает Зеленый (500 мс).
     */
    final private byte[] mess = new byte[4];

    /**
     * Тут вся логика работы кнопок и их нажатия
     *
     * @param b это команда от устройства
     */
    void doAction(byte b) {
        System.out.println("Device: userID=" + userId + " Name=" + user + " address=" + (addres.getBytes()[0] & 0xFF));
        if (b == 0x31) {
            System.out.println("b == 0x31 -- 49");
        } else if (b == 0x32) {
            System.out.println("b == 0x32 -- 50");
        } else if (b == 0x33) {
            System.out.println("b == 0x33 -- 51");
        } else if (b == 0x34) {
            System.out.println("b == 0x34 -- 52");
        } else {
            System.out.println("PIZDEzzzz.....");
        }
        if (user == null) {
            System.out.println("_user == null");
        } else {
            if (user.getShadow() == null) {
                System.out.println("user.getShadow() == null");
            } else {
                if (user.getShadow().getCustomerState() == null) {
                    System.out.println("user.getShadow().getCustomerState() == null");
                } else {
                    System.out.println("user.getShadow().getCustomerState() == " + user.getShadow().getCustomerState());
                }
            }
        }

        final CmdParams params = new CmdParams();
        params.userId = userId;
        if (user != null) {

            System.out.println("_Button Data=" + ((long) b) + " b=" + b);
            params.responseId = RespProp.getInstance().getResp(new Long(b)); // тут чего???
            System.out.println("!SEND RESP = " + params.responseId);
            params.textData = user.getCustomer() != null ? user.getCustomer().getFullNumber() : "";

            if (user.getShadow() != null) {
                System.out.println("IdOldService=" + user.getShadow().getIdOldService() + " IdOldCustomer=" + user.getShadow().getIdOldCustomer());
                params.serviceId = user.getShadow().getIdOldService();
                params.customerId = user.getCustomer() == null ? (user.getShadow().getIdOldCustomer()) : null;// в БД не записалось пока не завершили работу
                params.textData = user.getCustomer() != null ? user.getCustomer().getFullNumber() : user.getShadow().getOldCustomer().getFullNumber();
            }

            /*
             if (forStop == null ? params.textData == null : forStop.equals(params.textData)) {
             return;
             }

             forStop = params.textData;
             /*
             if (user.getCustomer() != null && (user.getCustomer().getState() == STATE_WORK || user.getCustomer().getState() == CustomerState.STATE_WORK_SECONDARY)) {
             params.serviceId = user.getCustomer().getService().getId();
             params.customerId = user.getCustomer().getId();
             params.textData = user.getCustomer().getInput_data() == null ? "" : user.getCustomer().getInput_data();
             }
             */
            System.out.println("-->");
            ITask task = Executer.getInstance().getTasks().get(Uses.TASK_SET_RESPONSE_ANSWER);
            System.out.println("--");
            final AJsonRPC20 result = (AJsonRPC20) task.process(params, "", new byte[4]);
            System.out.println("<--");
            if (result instanceof JsonRPC20Error) {
                System.out.println("ERROR! FeedbackBusRS485Plugin can not send a data to DB");
            } else {
                System.out.println("OK. FeedbackBusRS485Plugin");
            }

        }
        beReadyBeep();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
        }
        lightDown();
    }

    private void beReady() {
        System.out.println("beReady()");
        mess[2] = 0x34;// – мигает Зеленый (200 мс);
        Start.sendToDevice(mess);
    }

    private void beReadyBeep() {
        System.out.println("beReadyBeep()");
        mess[2] = 0x3B; // – писк (500 мс) + мигает Зеленый (200 мс);
        Start.sendToDevice(mess);
    }

    private void lightDown() {
        System.out.println("lightDown()");
        mess[2] = 0x30;// – светодиод погашен;
        Start.sendToDevice(mess);
    }

}
