/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.fbus485.core;

import java.util.Arrays;

/**
 * Класс содержит код для распараллеливаия обработки пришедшего пакета
 *
 * @author Evgeniy Egorov
 */
public class ActionTransmit implements Runnable {

    private byte[] bytes;

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public ActionTransmit() {
        this.bytes = new byte[0];
    }

    @Override
    public void run() {
        if (bytes.length == 4 && bytes[0] == 0x01 && bytes[3] == 0x07) {
            // должно быть 4 байта, иначе коллизия
            byte[] t = new byte[1];
            t[0] = bytes[1];
            String key = new String(t);
            final ButtonDevice dev = AddrProp.getInstance().getAddrByRSAddr(key);
            if (dev == null) {
                System.err.println("Anknown address from user device. bad='" + key + "'");
            } else {
                dev.doAction(bytes[2]);
            }
        } else {
            if (bytes.length == 19 && bytes[0] == 0x02 && bytes[18] == 0x03) {
                // должно быть 19 байт, иначе коллизия
                byte[] t = Arrays.copyOfRange(bytes, 4, 10);
                String key = new String(t);
                final ButtonDevice dev = AddrProp.getInstance().getAddrByRSAddr(key);
                if (dev == null) {
                    System.err.println("Anknown address from user device. bad='" + key + "'");
                } else {
                    dev.doAction(bytes[1]);
                }
            } else {
                System.err.println("Collision! Package lenght is not 4 bytes.");
            }
        }
    }
}
