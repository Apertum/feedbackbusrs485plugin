/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.fbus485.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Evgeniy Egorov
 */
public class RespProp {

    // byte -> response_id
    final private HashMap<Long, Long> resps = new HashMap<>();

    public HashMap<Long, Long> getResps() {
        return resps;
    }
    final static private File addrFile = new File("config/FeedbackBusRS485Plugin.resp");

    private RespProp() {
        try (FileInputStream fis = new FileInputStream(addrFile); Scanner s = new Scanner(fis)) {
            while (s.hasNextLine()) {
                final String line = s.nextLine().trim();
                if (!line.startsWith("#")) {
                    final String[] ss = line.split("=");
                    resps.put(Long.valueOf(ss[1]), Long.valueOf(ss[0]));
                }
            }
        } catch (IOException ex) {
            System.err.println(ex);
            throw new RuntimeException(ex);
        }
    }

    public static RespProp getInstance() {
        return AddrPropHolder.INSTANCE;
    }

    private static class AddrPropHolder {

        private static final RespProp INSTANCE = new RespProp();
    }

    public Long getResp(Long butData) {
        return resps.get(butData);
    }

    public static void main(String[] ss) {
        System.out.println("responces:");
        for (Long l : getInstance().resps.keySet()) {
            System.out.println("_"+(l) + "=" + getInstance().getResp(l));
        }
    }
}
