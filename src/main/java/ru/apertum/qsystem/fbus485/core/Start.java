/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.fbus485.core;

import gnu.io.SerialPortEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import ru.apertum.qsystem.extra.IStartServer;
import ru.evgenic.rxtx.serialPort.IReceiveListener;
import ru.evgenic.rxtx.serialPort.ISerialPort;
import ru.evgenic.rxtx.serialPort.RxtxSerialPort;

/**
 *
 * @author Evgeniy Egorov
 */
public class Start implements IStartServer {

    File propFile = null;
    Properties props = null;
    private static ISerialPort port;

    public static synchronized void sendToDevice(byte[] b) {
        try {
            String s = "";
            for (byte b1 : b) {
                s = s + (b1 & 0xFF) + "_";
            }
            System.out.print("<<" + s + " ... ");
            port.send(b);
        } catch (Exception ex) {
            System.err.println("ERR DEV " + ex);
        }
    }

    /**
     * пул потоков для работы с командами отоператоров
     */
    public ExecutorService es;

    /**
     * Ключ блокировки для манипуляции с кстомерами
     */
    public static final Lock receprtTaskLock = new ReentrantLock();

    private void initCOM() throws Exception {
        FileInputStream fis;
        try {
            propFile = new File("config/FeedbackBusRS485Plugin.properties");
            fis = new FileInputStream(propFile);
            props = new Properties();
            props.load(fis);
            fis.close();
        } catch (IOException ex) {
            System.err.println(ex);
            return;
        }
        try {
            port = new RxtxSerialPort(props.getProperty("port.name", "COM1"));
        } catch (Exception ex) {
            System.err.println("Порт не создался. " + ex);
            throw new RuntimeException(ex);
        }
        port.setSpeed(Integer.parseInt(props.getProperty("port.speed", "6900")));
        port.setDataBits(Integer.parseInt(props.getProperty("port.bits", "8")));
        port.setParity(props.getProperty("port.parity", "0").equals("1") ? 1 : 0);
        port.setStopBits(Integer.parseInt(props.getProperty("port.stopbits", "1")));

        es = Executors.newFixedThreadPool(24);
        port.bind(new IReceiveListener() {

            @Override
            public void actionPerformed(SerialPortEvent spe, final byte[] bytes) {
                // синхронизация работы с клиентом
                receprtTaskLock.lock();
                String s = "";
                for (byte b : bytes) {
                    s = s + (b & 0xFF) + "_";
                }
                System.out.println(">>" + s + "|" + new String(bytes));
                try {
                    final ActionTransmit aTransmitter = ActionRunnablePool.getInstance().borrowTransmitter();
                    try {
                        aTransmitter.setBytes(bytes);
                        es.submit(aTransmitter);
                    } finally {
                        ActionRunnablePool.getInstance().returnTransmitter(aTransmitter);
                    }
                } catch (Exception ex) {
                    throw new RuntimeException("Ошибка при принятии пакета и работы с ним." + ex);
                } finally {
                    receprtTaskLock.unlock();
                }
            }

            @Override
            public void actionPerformed(SerialPortEvent spe) {
            }
        });

    }

    @Override
    public void start() {
        final Start run = new Start();
        try {
            run.initCOM();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public String getDescription() {
        return "Plugin for feedback. Data bus RS485 architure is present.";
    }

    @Override
    public long getUID() {
        return 13;
    }

}
