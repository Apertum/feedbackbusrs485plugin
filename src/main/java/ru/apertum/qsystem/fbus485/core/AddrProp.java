/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.fbus485.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Evgeniy Egorov
 */
public class AddrProp {

    final private HashMap<Long, ButtonDevice> addrs = new HashMap<>();

    public HashMap<Long, ButtonDevice> getAddrs() {
        return addrs;
    }
    final static private File addrFile = new File("config/FeedbackBusRS485Plugin.adr");

    private AddrProp() {
        try (FileInputStream fis = new FileInputStream(addrFile); Scanner s = new Scanner(fis)) {
            while (s.hasNextLine()) {
                final String line = s.nextLine().trim();
                if (!line.startsWith("#")) {
                    final String[] ss = line.split("=");
                    addrs.put(Long.valueOf(ss[0]), new ButtonDevice(Long.valueOf(ss[0]), ss[1]));
                }
            }
        } catch (IOException ex) {
            System.err.println(ex);
            throw new RuntimeException(ex);
        }
    }

    public static AddrProp getInstance() {
        return AddrPropHolder.INSTANCE;
    }

    private static class AddrPropHolder {

        private static final AddrProp INSTANCE = new AddrProp();
    }

    public ButtonDevice getAddr(Long userId) {
        return addrs.get(userId);
    }

    public ButtonDevice getAddrByRSAddr(String rsAddr) {
        for (ButtonDevice adr : AddrProp.getInstance().getAddrs().values().toArray(new ButtonDevice[0])) {
            if (adr.addres.equals(rsAddr)) {
                return adr;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        String s = "C1C6f8";
        System.out.println(s);
        byte[] ss = s.getBytes();
        System.out.println(Arrays.toString(ss));
        String s2 = new String(ss);
        System.out.println(s2);

        for (byte i = -128; i < 127; i++) {
            byte[] t = new byte[1];
            t[0] = i;
            System.out.println((i & 0xFF) + "=" + new String(t));

        }

        /*
         System.out.println("addrs:");
         for (Long l : getInstance().addrs.keySet()) {
         System.out.println(l + "=" + getInstance().getAddr(l).addres);
         }
         */
    }
}
